﻿using UnityEngine;
using System.Collections;

public class Antigravity : MonoBehaviour {


	public bool isActive;
	private Rigidbody rb;
	private bool shiftPressed;

	private static Vector3 pushDirection;

	void Start() {
		rb = GetComponent<Rigidbody> ();
		shiftPressed = false;
	}

	void Update() {
		if (Input.GetKeyDown (KeyCode.Tab))
			isActive = !isActive;
		if (Input.GetKeyDown (KeyCode.LeftShift))
			shiftPressed = !shiftPressed;

		if (!shiftPressed && Input.GetMouseButtonDown (0)) {
			if (transform.GetChild (0) != null)
				pushDirection = transform.GetChild (0).forward;
			rb.AddForce (pushDirection * 10, ForceMode.VelocityChange);
		}
		else if (shiftPressed && Input.GetMouseButton (0)) {
			if (transform.GetChild (0) != null)
				pushDirection = transform.GetChild (0).forward;
			rb.AddForce (pushDirection * 200, ForceMode.Force);
		}
	}

	void FixedUpdate () {
		if (isActive)
			rb.AddForce (-Physics.gravity * 1.0003f, ForceMode.Acceleration);
	}
}
